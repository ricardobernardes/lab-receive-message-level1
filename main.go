package main

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type Message struct {
	Id       uint 	`json:"id"`
	Name     string `json:"name"`
	Age  	 uint 	`json:"age"`
}

func receive(event *events.SQSEvent) error{

	for _, message := range event.Records {
		snsEntity := events.SNSEntity{}
		err := json.Unmarshal([]byte(message.Body), &snsEntity)
		if err != nil {
			fmt.Println("Error unmarshal: ", err)
			continue
		}

		msg := Message{}
		err = json.Unmarshal([]byte(snsEntity.Message), &msg)
		if err != nil {
			fmt.Println("Error unmarshal dataInvoice: ", err)
			continue
		}

		jsonMsg, _ := json.Marshal(msg)
		fmt.Println("Message: ", string(jsonMsg))

	}

	return nil

}

func main(){
	lambda.Start(receive)
}
